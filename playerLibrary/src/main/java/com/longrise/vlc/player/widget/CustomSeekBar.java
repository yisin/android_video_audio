package com.longrise.vlc.player.widget;

import android.content.Context;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by luoyang on 2017/5/25.
 */

public class CustomSeekBar extends AppCompatSeekBar {

    private boolean mIsSeekAble;

    public CustomSeekBar(Context context) {
        super(context);
    }

    public CustomSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setSeekAble(boolean isSeekAble) {
        this.mIsSeekAble = isSeekAble;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mIsSeekAble ? super.onTouchEvent(event) : mIsSeekAble;
    }
}
