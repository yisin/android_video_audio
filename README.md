#Android音视频播放库V1.0.0
<br>
目前Android原生的VideoView等视频播放控件支持格式有限，且在部分机型上兼容性存在着问题，所以封装这个第三方库，该库根据VLC较新版本(2.0.2)进行封装（底层基于ffmpeg），目前支持音视频的在线、本地播放，也可自定义音视频播放界面，支持当今大多数媒体及视频格式。
<br><br>支持功能如下，欢迎提出问题进行修改。
####视频
* 支持单个视频的播放、暂停、播放进度条拖动、标题显示
* 支持主流视频格式
* 兼容性好，目前测试了主流版本和手机，暂时没发现问题，如果有问题请反馈给我进行修改
* 支持自定义播放界面，提供类似于VideoView对视频画面进行控制

####音频
* 支持单个音频播放、暂停
* 支持主流的音频格式
* 支持自定义播放器，提供音频控制器（AudioPlayerControl）对音频进行控制

####重要！！！使用之前请仔细阅读下面几点，避免踩坑.
* 对于6.0及以上系统（sdk>=23），在播放本地音视频之前，请先申请访问读取外部存储运行时权限（READ_EXTERNAL_STORAGE，WRITE_EXTERNAL_STORAGE）
* 目前支持的手机架构包括armeabi-v7a、armeabi-v8a，如果有用到极少数的x86、mips架构，请联系我加入相应架构的so库
* 对于buid.gradle中配置了ndk支持的架构，请小心使用，最好配置一个v7a支持，不然可能会有问题。
* 
		ndk {
            //设置支持的SO库架构
            abiFilters  'armeabi-v7a'//, 'arm64-v8a'
        }

##视频播放使用步骤 

### 1.使用默认播放器进行视频播放
	 //视频播放路径
 	String path = localVideoEt.getText().toString().trim();
	//播放界面所需参数对象
    PlayerPara para = new PlayerPara(path);
	//设置播放进度条不可拖动
    para.setDragSeekBar(false);
	//跳转播放界面进行视频播放
    VideoPlayerActivity.gotoVideoPlayerActivity(MainActivity.this, para);
<br>
######视频播放支持如下属性设置

    //播放地址 必传
    private String path;

    //标题 不传不进行显示
    private String title;

    //是否能拖动进度条 默认为true
    private boolean isDragSeekBar = true;

    //工具栏显示时 隐藏工具栏时间 单位毫秒 默认 8s
    private int hideOverLayLength = 8000;

######效果图如下：
![](http://i.imgur.com/LAaGq9Q.jpg)
### 2.自定义播放界面实现视频播放
######自己写好播放界面样式,引入自定义播放控件VideoView（提供了跟官方类似的功能）
	 <org.videolan.libvlc.media.VideoView
        android:id="@+id/pv_video"
        android:layout_width="match_parent"
        android:layout_height="match_parent" />
######设置播放路径，交由VideoView控制播放画面
 	if(isLocalPath(mPlayerPara.getPath())) {
		//设置本地播放路径
        mPlayerView.setVideoPath(mPlayerPara.getPath());
    } else {
		//设置在线播放路径
        mPlayerView.setVideoURI(Uri.parse(mPlayerPara.getPath()));
    }
	//设置播放画面状态监听
	mPlayerView.setOnChangeListener(this);
	//开始播放
	mPlayerView.start();
######VideoView提供了如下对视频操作接口

	//是否在播放
    boolean isPlaying();
	//是否能快进
    boolean isSeekAble();
	//播放
    void play();
	//暂停
    void pause();
	//停止
    void stop();
	//获取当前的播放状态
    int getPlayerState();
	//获取当前音量大小
    int getVolume();
	//调节音量
    int setVolume(int volume);
	//获取资源长度
    long getLength();
	//获取当前播放进度
    long getCurrentPosition();
	//资源释放
    void release();
	//跳到某个播放时间点
    void seekTo(int milliSeconds);
##音频播放使用步骤 

### 1.使用默认播放器进行音频播放
######引入自定义音频播放控件
 	 <com.longrise.vlc.player.widget.AudioPlayerView
        android:id="@+id/audio_player_view"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="100dp"
        />
######设置播放路径开始播放
	//获取音频播放路径
  	mAudioView = (AudioPlayerView)findViewById(R.id.audio_player_view);
	//设置音频播放路径，会自动判断本地或者在线路径
    mAudioView.setPath(mCurrentUrl);
	//开始播放
	mAudioView.start();
######效果图如下：
![](http://i.imgur.com/TR1d4Xa.jpg)

### 2.自定义播放界面实现音频播放
######自定义音频播放布局,初始化音频播放控制器，该控制器实现了上述所有的视频操作接口
	//初始化音频播放控制器，需传入播放path
	 mAudioPlayerControl = new AudioPlayerControl(mCurrentUrl);
	//设置音频播放监听
     mAudioPlayerControl.setOnChangeListener(this);
	//开始进行音频播放
	 mAudioPlayerControl.play();

#####由于第一个版本支持的功能不多，后续将支持下面列举的更多功能
####视频
* 支持对多个视频的播放
* 支持窗口横竖屏，放大缩小等
* 支持音量调节、亮度调节、快进手势操作
* 支持同一个视频分高清、普清等选择
* 支持屏幕锁定功能
####音频
* 支持多个音频播放
* 支持App退出后仍然能播放、播放界面显示在通知栏中