package longrise.vlcvideo;

import android.app.Application;

/**
 * Created by luoyang on 2017/5/24.
 */

public class VlcVideoApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //崩溃日志信息
        CrashUtils.getInstance().init(this);
    }
}
