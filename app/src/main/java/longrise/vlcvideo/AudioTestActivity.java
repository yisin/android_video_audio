package longrise.vlcvideo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.longrise.vlc.player.util.VlcUtils;
import com.longrise.vlc.player.widget.AudioPlayerView;

public class AudioTestActivity extends AppCompatActivity implements View.OnClickListener {

    private AudioPlayerView mAudioView;
    private boolean mFirstCome;
    //返回
    private RelativeLayout mBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_auto);
         String mCurrentUrl = getIntent().getStringExtra("url");
         mBack = (RelativeLayout) findViewById(R.id.rl_back_left);
        if(mBack != null) {
            mBack.setOnClickListener(this);
        }
         mAudioView = (AudioPlayerView)findViewById(R.id.audio_player_view);
         mAudioView.setPath(mCurrentUrl);
    }

    public static void gotoTestAudioPlayerActivity(Context mContext, String videoUrl) {

        if(VlcUtils.isEmpty(videoUrl)) {
            Toast.makeText(mContext, "视频播放地址有误", Toast.LENGTH_LONG);
            return ;
        }
        Intent intent = new Intent(mContext, AudioTestActivity.class);
        intent.putExtra("url", videoUrl);
        mContext.startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mAudioView.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAudioView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mFirstCome) {
            mFirstCome = false;
        } else {
            if(mAudioView.getPlayState()) {
                mAudioView.start();
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mAudioView != null) {
            mAudioView.destroy();
        }
    }

    @Override
    public void onClick(View v) {
        finish();
    }
}
