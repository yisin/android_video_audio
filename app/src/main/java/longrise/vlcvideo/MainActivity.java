package longrise.vlcvideo;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.longrise.vlc.player.activity.VideoPlayerActivity;
import com.longrise.vlc.player.model.PlayerPara;
import com.longrise.vlc.player.util.VlcUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity {

    @Bind(R.id.local_audio_et)
    EditText localAudioEt;
    @Bind(R.id.online_audio_et)
    EditText onlineAudioEt;
    @Bind(R.id.local_video_et)
    EditText localVideoEt;
    @Bind(R.id.online_video_et)
    EditText onlineVideoEt;
    String onlineVideo = "http://download.yxybb.com/bbvideo/web/d1/d13/d8/d2/f1-web.mp4";

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        String path = Environment.getExternalStorageDirectory() + "/";
        localVideoEt.setText(path);
        onlineVideoEt.setText(onlineVideo);
        if(Build.VERSION.SDK_INT < 23) {
            if(!VlcUtils.isEmpty(onlineVideoEt.getText().toString().trim())) {
                path = onlineVideoEt.getText().toString().trim();
                PlayerPara para = new PlayerPara(path);
                para.setDragSeekBar(true);
                VideoPlayerActivity.gotoVideoPlayerActivity(MainActivity.this, para);
            } else {
                Toast.makeText(MainActivity.this, "请输入在线视频地址", Toast.LENGTH_LONG);
            }
        } else {
            verifyStoragePermissions(this);
        }

    }

    @OnClick({R.id.local_audio, R.id.online_audio, R.id.local_video, R.id.online_video})
    public void onClick(View view) {
        String path ;
        switch (view.getId()) {
            case R.id.local_audio:
                if(!VlcUtils.isEmpty(localAudioEt.getText().toString().trim())) {
                    path = localAudioEt.getText().toString().trim();
                    AudioTestActivity.gotoTestAudioPlayerActivity(MainActivity.this, path);
                } else {
                    Toast.makeText(MainActivity.this, "请输入本地音频地址", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.online_audio:
                if(!VlcUtils.isEmpty(onlineAudioEt.getText().toString().trim())) {
                    path = onlineAudioEt.getText().toString().trim();
                    AudioTestActivity.gotoTestAudioPlayerActivity(MainActivity.this, path);
                } else {
                    Toast.makeText(MainActivity.this, "请输入在线音频地址", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.local_video:
                if(!VlcUtils.isEmpty(localVideoEt.getText().toString().trim())) {
                    path = localVideoEt.getText().toString().trim();
                    PlayerPara para = new PlayerPara(path);
                    para.setDragSeekBar(false);
                    VideoPlayerActivity.gotoVideoPlayerActivity(MainActivity.this, para);
                } else {
                    Toast.makeText(MainActivity.this, "请输入本地视频地址", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.online_video:
                path = onlineVideoEt.getText().toString().trim();
                PlayerPara para = new PlayerPara(path);
                para.setDragSeekBar(true);
                VideoPlayerActivity.gotoVideoPlayerActivity(MainActivity.this, para);
               /* if(!VlcUtils.isEmpty(onlineVideoEt.getText().toString().trim())) {

                } else {
                    Toast.makeText(MainActivity.this, "请输入在线视频地址", Toast.LENGTH_LONG).show();
                }*/
                break;
        }
    }

    public static void verifyStoragePermissions(Activity activity) {
        int permission = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE : {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if(!VlcUtils.isEmpty(onlineVideoEt.getText().toString().trim())) {
                        String path = onlineVideoEt.getText().toString().trim();
                        PlayerPara para = new PlayerPara(path);
                        para.setDragSeekBar(true);
                        VideoPlayerActivity.gotoVideoPlayerActivity(MainActivity.this, para);
                    } else {
                        Toast.makeText(MainActivity.this, "请输入在线视频地址", Toast.LENGTH_LONG).show();
                    }
                } else {

                    finish();
                }
                return;
            }
        }
    }
}
